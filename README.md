# provaDAM

## LENGUAJE NIVEL BAJO

### Definición y características

Un lenguaje de bajo nivel son **instrucciones** que ensamblan los grupos de conmutadores necesarios para expresar una mínima lógica aritmética y contienen tantas instrucciones como la arquitectura del hardware así haya sido diseñada.

Los lenguajes de nivel bajo son dependientes de la máquina, es decir que el programa que se realiza con este tipo de lenguajes no se pueden migrar o utilizar en otras máquinas y por norma general están **disponibles a nivel firmware o ship set**.

Estos lenguajes están **orientados a procesos**, que se componen de tareas.
Se trata de un **código sencillo** con **instrucciones directas**, de **fácil conversión** y es muy **adaptable**, además de que trabaja a **gran velocidad**.

### Tipos

* Código binario
* Lenguaje máquina
* Lenguaje ensamblador


### Ejemplos

C y C++ (nivel medio pero se pueden utilizar para programar a nivel bajo), Basic...

![Foto.](https://i0.wp.com/www.diarlu.com/wp-content/uploads/2018/10/tipos-lenguajes-programacion.jpg?resize=961%2C518&ssl=1 "Imagen.")

## Lenguajes de alto nivel

### Definición

Son aquellos que se encuentran más cercanos al lenguaje natural que al lenguaje máquina. Se tratan de lenguajes independientes de la arquitectura del ordenador. Por lo que, en principio, un programa escrito en un lenguaje de alto nivel, lo puedes migrar de una máquina a otra sin ningún tipo de problema.

Estos lenguajes permiten al programador olvidarse por completo del funcionamiento interno de la maquina/s para la que están diseñando el programa.

Suelen usar tipos de datos para la programación y hay lenguajes de propósito general y de propósito especifico.

### Ejemplos

C++, FORTRAN, C Sharp, Pascal, Python.

```python
s = "Python syntax highlighting"
print s
```
